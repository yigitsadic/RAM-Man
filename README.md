# RAM-Man

![RAM-icon](favicon.ico)

Bu proje İşletim Sistemleri dersi için RAM yönetimi uygulamasıdır.

## Çalışma Mantığı

RAM'in belirli bir kısmı işletim sistemine ayırılmıştır.
Çalıştırılacak uygumalar ram'in ilk satırlarına yazılamazlar. Burası işletim
sisteminindir.

Uygulama çok ilkel bir şekilde, bir işletim sisteminin RAM'i nasıl kontrol
ettiğini taklit eder.

Bir uygulama çalıştırıldığında eğer yeteri kadar boş alan varsa, RAM'e yüklenir.
RAM'de olduğu adres, işletim sisteminin kapladığı yerden sonra uygulamanın
boyutunun eklenmesiyle elde edilir.

## Uygulama Nasıl Çalışacak?

Taklit edilen bilgisayar 1024*5 MB RAM'e sahiptir.

Çalıştırılacak uygulama ve çalışma süresi seçilecek, ve bu süre sayaç ile
gösterilecektir.

Uygulama için bazı sabitler vardır. Bunlar aşağıda listelenmiştir.


| Uygulama Adı    | Boyutu (MB) |
| --------------- | ----------- |
| İşletim Sistemi | 2000        |
| Kelime İşlemci  | 400         |
| Tarayıcı        | 600         |
| Video Oynatıcı  | 300         |
| Oyun            | 2000        |
| Mail İstemcisi  | 150         |
