var processes = [
  {
    id: 1,
    name: 'Office: Word',
    size: '400',
    image_url: 'assets/images/Word-icon.png'
  },
  {
    id: 2,
    name: 'Firefox',
    size: '600',
    image_url: 'assets/images/firefox_icon.png'
  },
  {
    id: 3,
    name: 'VLC Player',
    size: '300',
    image_url: 'assets/images/vlc_icon.png'
  },
  {
    id: 4,
    name: 'Minecraft',
    size: '2000',
    image_url: 'assets/images/minecraft_icon.png'
  },
  {
    id: 5,
    name: 'Thunderbird',
    size: '150',
    image_url: 'assets/images/thunderbird_icon.png'
  }
];

angular.module('ram-man', ['timer']);

angular.module('ram-man').controller('MainCtrl', [
  '$scope', '$timeout',
  function($scope, $timeout) {
    // Defaults...
    $scope.started = false;

    // Run animation
    $scope.running = false;

    // Ram overflowed?
    $scope.ram_overflow = false;

    // Default ram capacity
    $scope.ram_capacity = 1024 * 5;

    // OS ram consumption
    $scope.ram_used = 2048;

    // Used ram percentage.
    $scope.ram_percentage = Math.round(($scope.ram_used * 100) / $scope.ram_capacity);

    // Available processes injected.
    $scope.available_processes = processes;

    // Process queue
    $scope.process_queue = [];

    // Process queue empty?
    $scope.process_queue_empty = true;

    // Default action id
    $scope.action_id = 0;

    $scope.current_queue_step = 0;

    // Actions starts with OS booting up.
    $scope.actions = [
      {
        id: $scope.action_id++,
        content: 'Ubuntu 16.10 başlatıldı.'
      }
    ];

    $scope.select_app = function(obj) {
      $scope.work_name = obj.name;
      $scope.selected  = obj.id;
    }

    $scope.addToQueue = function() {
      if ($scope.selected === '' || $scope.proc_work_time === '' || (!$scope.proc_work_time) || parseInt($scope.proc_work_time) <= 1) {
        $scope.ram_overflow = true;
        return;
      }

      // Fetch process
      var selected_process = $scope.available_processes[$scope.selected-1];

      var total_time = 1;

      angular.forEach($scope.process_queue, function(obj) {
        total_time += parseInt(obj.work_time);
      });

      // Add process to queue
      $scope.process_queue.push({
        work_time: $scope.proc_work_time,
        name:      selected_process.name,
        size:      selected_process.size,
        image_url: selected_process.image_url,
        actual_work_time: total_time + parseInt($scope.proc_work_time)
      });

      $scope.process_queue_empty = false;
    }

    $scope.run_processes = function() {
      $scope.started = true;

      // Show animation
      $scope.running = true;

      var total_time = 1;

      angular.forEach($scope.process_queue, function(obj) {
        total_time += parseInt(obj.work_time);
      });

      angular.forEach($scope.process_queue, function(obj) {
        $timeout(function() {
          $scope.actions.push({
            content: obj.name + ' bitirildi.'
          });

          $scope.current_queue_step = ($scope.process_queue.indexOf(obj)) + 1;
        }, parseInt(obj.actual_work_time) * 1000);
      });

      angular.forEach($scope.process_queue, function(obj) {
        $timeout(function() {
          $scope.actions.push({
            content: obj.name + ' başlatıldı.'
          });

          $scope.current_process = obj;

          $scope.ram_used = 2048 + parseInt(obj.size);
          $scope.ram_percentage = Math.round(($scope.ram_used * 100) / $scope.ram_capacity);
        }, (parseInt(obj.actual_work_time) - parseInt(obj.work_time)) * 1000);
      });

      $timeout(function() {
        $scope.running = false;
        $scope.ram_used = 2048;
        $scope.ram_percentage = Math.round(($scope.ram_used * 100) / $scope.ram_capacity);
      }, total_time * 1000);
    }
  }
]);
